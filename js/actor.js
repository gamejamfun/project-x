var Actor = new Phaser.Class({

    Extends: Phaser.GameObjects.Sprite,

    initialize:
    // Actor Constructor
    function Actor (scene, name , x, y, atlas, png, animation) {
        
        Phaser.GameObjects.Sprite.call(this, scene, x, y, atlas, png);
        if (animation) {
            this.play(animation);
        }
        scene.add.existing(this);
        this.name = name;
        this.blood = 100;
        this.atk = 1;
        this.alive = true;
    },

    SetHp:function (hp) {
        this.blood = hp;
        if (this.blood <= 0) {
            this.blood = 0;
            this.alive = false;
        }
        
        this.bloodBar.clear();
        this.bloodBar.fillStyle(0xff0000, 1);
        this.bloodBar.fillRect(this.width/2*-1, this.height/2*-1, this.width*(this.blood/100), 10);
    },

    Injured: function(damage) {
        if (!this.alive) {
            return;
        }

        this.blood -= damage;
        if (this.blood <= 0) {
            this.blood = 0;
            this.alive = false;
        }
        this.bloodBar.clear();
        this.bloodBar.fillStyle(0xff0000, 1);
        this.bloodBar.fillRect(this.width/2*-1, this.height/2*-1, this.width*(this.blood/100), 10);
    },

    IsAlive: function() {
        return this.alive;
    },

    GetBlood: function() {
        return this.blood;
    },



    Attack:function (enemy) {

    }, 

});
