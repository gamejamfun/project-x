// 網路
var ws;
function WebSocketTest(game) {

    var playground_id;
    if ("WebSocket" in window) {
        // alert("WebSocket is supported by your Browser!");

        // Let us open a web socket
        console.log('start connect');
        ws = new WebSocket("ws://gamejam.b91.work:8001/ws");
        // ws = new WebSocket("ws://127.0.0.1:8000/ws");

        ws.onerror = function (err) {
            console.log(`onerror: ${err}`);
        }

        ws.onopen = function() {
            console.log("Connection is connected...");
            setPlayground();
        };

        ws.onmessage = function (evt) {
            var received_msg = evt.data;
            
            var obj = JSON.parse(received_msg);
            console.log(obj);
            // console.log(JSON.stringify(obj));
            // console.log(obj);
            switch(obj.type) {
                case 'action': 
                // alert('dfdf')
                    onRevActions(obj);
                break;
                case 'Shoot':
                    onRevShoot(obj);
                break;
                case 'join':
                    onSpawnPlayer(obj.message);
                break;
            }

        };

        ws.onclose = function() {
            console.log("websocket disconnect");
        };
    } else {

        // The browser doesn't support WebSocket
        console.log("WebSocket NOT supported by your Browser!");
    }

    function onRevActions(jsonObj) {
        let messageStr = jsonObj.message;
        let messageInfo = JSON.parse(messageStr);
        let type = messageInfo.type;
        // alert(type);
        switch(type) {
            case 'position':
                actionPosition(messageInfo);
                break;
            case 'HitBoss':
                actionHitBose(messageInfo);
                break;
            case 'HP':
                actionHp(messageInfo);
            break;
            case 'gamestart':
                actionGameStart(messageInfo);
            break;
        }

    }

    function actionPosition(actionJson) {
        let data = JSON.parse(actionJson.data);
        let ID = data.ID;
        let x = data.x|0;
        let y = data.y|0;
        console.log(data);
        if (ID === 'Engineer') {
            enemy.setPosition(x, y);
        } else {
            players.children.entries.forEach(function(obj){
                if(obj.name === ID)
                {   
                   obj.setPosition(x, y);
                }
            });
        }
    }

      ws.actionHitBose = function () {
        let messageInfo = {
            'type':'HitBoss',
            'data':''
        }
        let data = {
            'type':'action',
            'playground_id':`${playground_id}`,
            'message': JSON.stringify(messageInfo)
        }

        return JSON.stringify(data);
    }


    function sendHitBose(actionJson) {


        // {"type":"action","playground_id":"3","message":"{\"type\":\"HitBoss\",\"data\":\"\"}"}
    }
    function actionHp(actionJson) {
        let data = JSON.parse(actionJson.data);
        for(idx in data.HP) {
            var name = data.playerID[idx];
            var hp = data.HP[idx];
            if (name === 'Engineer') {
                enemy.getByName('player').SetHp(hp);
            } else{
                players.children.entries.forEach(function(obj){
                    if(obj.name === name) 
                    {   
                       obj.getByName('player').SetHp(hp);
                    }
                });
            }
        }        
        
    }

    function actionGameStart(actionJson) {
        // just start game
        
    }

    function onRevShoot(obj) {
        
    }

    ws.doSendPosition = function(x, y) {
        let messageData = {
            'ID': `${game.ClientName}`,
            'x':`${x}`,
            'y':`${y}`
        }

        let messageInfo = {
            'type':'position',
            'data':(JSON.stringify(messageData))
        }
        let data = {
            'type':'action',
            'playground_id':`${playground_id}`,
            'message': (JSON.stringify(messageInfo))
        }

        return JSON.stringify(data);
    }


    /**
     * hit booooooos
     */
    ws.doSendHitBoss = function(){
        let messageInfo = {
            'type':'HitBoss'
        }
        let data = {
            'type':'action',
            'playground_id':`${playground_id}`,
            'message': JSON.stringify(messageInfo)
        }

        return JSON.stringify(data);
        // {"type":"action","playground_id":"3","message":"{\"type\":\"HitBoss\",\"data\":\"\"}"}
        // {"type":"action","playground_id":"3","message":"{\"type\":\"HP\",\"data\":\"{\\\"HP\\\":[100,100],\\\"playerID\\\":[\\\"abc\\\",\\\"abc\\\"]}\"}"}
    }
    function onSpawnPlayer(json) {
        console.log(json);
        var $players = JSON.parse(json);
        $players.forEach(function($player){
            if ($player.Name !== 'Engineer') {
                var search = players.children.entries.filter(function(obj){
                    return obj.name === $player.Name;
                });
                if (search.length > 0) return;
                var user = spawnPlayer(game, 100, 100, $player.Name, 'sprites/drunk_01.png', 'player_idle', false);
                user.MExplosion = game.sound.add('m_explosion', {
                    volume: 0.1
                });
                if (game.ClientName === $player.Name) {
                    player = user;
                }
                players.add(user);
                players.children.entries.forEach(function(obj){
                    obj.setScale(0.4, 0.4);
                    obj.body.setCollideWorldBounds(true);
                    console.log(obj);
                });
                console.log(search);
            } else {
                var search = enemies.children.entries.filter(function(obj){
                    return obj.name === $player.Name;
                });
                if (search.length > 0) return;
                enemy = spawnPlayer(game, 250, 250, $player.Name, 'sprites/boss_01.png', 'enemy_idle', true);
                // enemy = spawnPlayer(this, 250, 250, 'Sean', 'role/role_a_frame-1.png', 'player_idle', true);
                enemy.scaleTween = game.tweens.add({
                    targets: enemy,
                    scaleX : 0.4,
                    scaleY : 0.4,
                    // alpha: 0.5,
                    ease: 'Linear',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
                    duration: 200,
                    repeat: 0,            // -1: infinity
                    yoyo: true,
                    paused: true
                });
                enemies.add(enemy);
                enemies.children.entries.forEach(function(obj){
                    obj.setScale(0.5, 0.5);
                    obj.body.setCollideWorldBounds(true);
                });
            }
        });
    }

    
    function setPlayground() {
        console.log(window.location)
        let urlParams = new URL(window.location.href);
        let param = urlParams.searchParams;
        let name = param.get('name');
        let pg = param.get('playground_id');
        playground_id = pg;
        game.ClientName = name;

        sendMsg(JSON.stringify({
            type: "login",
            playground_id: pg,
            message: name
        }));
    }

    function sendMsg(msg) {
        console.log(ws)
        ws.send(msg)
    }
}