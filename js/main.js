//  遊戲配置
var config = {
    type: Phaser.AUTO,
    width: 500,
    height: 500,
    physics: {
        default: 'arcade',
        arcade: {
            // debug: true,
            // gravity: { y: 600 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    },
    parent: "app"
};


var player, enemy;
var players, enemies;
var MGlassBrk;

// 遊戲設定
var game = new Phaser.Game(config);

// 遊戲素材載入
function preload() {
    var progress = this.add.graphics();
    this.load.on('progress', function(value) {
        progress.clear();
        progress.fillStyle(0xffffff, 1);
        progress.fillRect(0, 270, 800 * value, 60);
    });

    this.load.on('complete', function() {
        progress.destroy();
    });
    //
    this.load.setPath('assets/');
    this.load.multiatlas('atlas', 'assets.json', 'assets');
    // music
    this.load.audio('m_explosion', 'audio/explosion.wav');
    this.load.audio('m_glassbrk', 'audio/glassbrk.wav');
}

// 角色人物場景配置

function create() {
    var background = this.add.sprite(0, 0, 'atlas', 'sprites/road_scene.png');
    this.physics.world.setBoundsCollision(true, true, true, true);
    this.physics.world.on('worldbounds', function(body){
        if (! body.gameObject.visible) {
            return ;
        }
        var ex = this.add.sprite( body.gameObject.x,  body.gameObject.y, 'atlas', 'sprites/beer_broken_01.png').play('beer_broken');
        ex.setScale(0.5, 0.5);
        body.gameObject.clear();
        MGlassBrk.play();
    },this);
    
    setupKeyboard(this);
    createAnimations(this);

    players = this.physics.add.group();

    // // player 
    // // player = spawnPlayer(this, 250, 125, 'Xuan', 'hurt/role_a_frame-1.png', 'enemy_idle', false);
    // player = spawnPlayer(this, 250, 125, 'Xuan', 'sprites/drunk_01.png', 'player_idle', false);
    // player.MExplosion = this.sound.add('m_explosion', {
    //     volume: 0.1
    // });

    // players.add(player);

    // players.children.entries.forEach(function(obj){
    //     obj.setScale(0.4, 0.4);
    //     obj.body.setCollideWorldBounds(true);
    // })

    MGlassBrk = this.sound.add('m_glassbrk', {
        volume: 0.1
    });

    enemies = this.physics.add.group();
    
    // enemy = spawnPlayer(this, 250, 250, 'Sean', 'sprites/boss_01.png', 'enemy_idle', true);
    // // enemy = spawnPlayer(this, 250, 250, 'Sean', 'role/role_a_frame-1.png', 'player_idle', true);
    // enemy.scaleTween = this.tweens.add({
    //     targets: enemy,
    //     scaleX : 0.4,
    //     scaleY : 0.4,
    //     // alpha: 0.5,
    //     ease: 'Linear',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
    //     duration: 200,
    //     repeat: 0,            // -1: infinity
    //     yoyo: true,
    //     paused: true
    // });

    // enemies.add(enemy);

    // enemies.children.entries.forEach(function(obj){
    //     obj.setScale(0.5, 0.5);
    //     obj.body.setCollideWorldBounds(true);
    // });
    //
    WebSocketTest(this);
    // 遊戲開始
    gameStart(self);
}

// var bulletTick = 0;
// 遊戲邏輯
function update(time, deltaTime) {
    // bulletTick+= deltaTime;
    // if (bulletTick >= 100) {
    //     let bullet = playerBullets.get();
    //     if (bullet) {
    //         bullet.setActive(true).setVisible(true);
    //         bullet.fire(player, { x: enemy.x  , y: enemy.y });
    //     }
    //     bulletTick = 0;
    // }

    // playerBullets.children.entries.forEach(function(obj){
    //     obj.setScale(0.01, 0.01);
    //     obj.setSize(0.01, 0.01);
    //     obj.body.setCollideWorldBounds(true);
    //     obj.body.onWorldBounds = true;
    //     // obj.body.world.on('worldbounds', function(body) {
    //     //     if (body.gameObject === this) {
    //     //        this.clear();
    //     //        playerBullets.remove(this);
    //     //     }
    //     //   }, obj);
    // })
    if (WKey.isDown ) {
        player.y -= deltaTime ;
        ws.send(ws.doSendPosition(player.x, player.y));
    } 
    
    if (SKey.isDown ) {
        player.y += deltaTime ;
        ws.send(ws.doSendPosition(player.x, player.y));
    } 
    
    if (AKey.isDown ) {
       player.x -= deltaTime;
       ws.send(ws.doSendPosition(player.x, player.y));
    } 
     
    if (DKey.isDown ) {
        player.x += deltaTime;
        ws.send(ws.doSendPosition(player.x, player.y));
     }


    if (SpaceKey.isDown) {
        player.fire(enemy);


        // players.getChildren().forEach(function (obj) {
        //     obj.fire(enemy, time);
        // })

        // players.getChildren().forEach(function (obj) {
        //     //  

        //     if (obj.name === 'Xuan') {
        //         console.log(obj.getByName('player'));
        //         var bloodBar = obj.getByName('blood');
        //         obj.getByName('player').Injured(1, bloodBar);
        //     }
        // })
    }
}



// 遊戲開始
function gameStart() {

}

// 建立動畫
function createAnimations(self) {
    // 醉漢動作
    var playerFrame = self.anims.generateFrameNames('atlas', {
        start: 1, end: 2, zeroPad: 0,
        prefix: 'sprites/drunk_0', suffix: '.png'
    });
    self.anims.create({ key: 'player_idle', frames: playerFrame, frameRate: 10, repeat: -1 });
    // Boss動作
    var enemyFrame = self.anims.generateFrameNames('atlas', {
        start: 1, end: 2, zeroPad: 0,
        prefix: 'sprites/boss_0', suffix: '.png'
    });
    self.anims.create({ key: 'enemy_idle', frames: enemyFrame, frameRate: 10, repeat: -1 });
    // 酒瓶破掉
    var beerBroken = self.anims.generateFrameNames('atlas', {
        start: 1, end: 3, zeroPad: 0,
        prefix: 'sprites/beer_broken_0', suffix: '.png'
    });
    self.anims.create({ key: 'beer_broken', frames: beerBroken, frameRate: 9, repeat: 0, hideOnComplete: true});

    // var alien1Frame = self.anims.generateFrameNames('alienscene', {
    //     start: 1, end: 3, zeroPad: 0,
    //     prefix: 'alien/alien1-', suffix: '.png'
    // });

    // self.anims.create({ key: 'alien1_idle', frames: alien1Frame, frameRate: 10, repeat: -1 });
    

    // var alien2Frame = self.anims.generateFrameNames('alienscene', {
    //     start: 1, end: 3, zeroPad: 0,
    //     prefix: 'alien/alien2-', suffix: '.png'
    // });

    // self.anims.create({ key: 'alien2_idle', frames: alien2Frame, frameRate: 10, repeat: -1 });

    // // 流星

    // var fallingStar = self.anims.generateFrameNames('alienscene', {
    //     start: 1, end: 13, zeroPad: 0,
    //     prefix: 'star/star', suffix: '.png'
    // });

    // self.anims.create({ key: 'falling_star', frames: fallingStar, frameRate: 9, repeat: 0, hideOnComplete: true});

    // // 吃到字母
    // var eatingEffect = self.anims.generateFrameNames('alienscene', {
    //     start: 0, end: 7, zeroPad: 0,
    //     prefix: 'explosion/explosion', suffix: '.png'
    // });

    // self.anims.create({ key: 'eating_effect', frames: eatingEffect, frameRate: 9, repeat: 0, hideOnComplete: true});
    // // Dog
    // var dogAttack = self.anims.generateFrameNames('alienscene', {
    //     start: 1, end: 3, zeroPad: 0,
    //     prefix: 'animals/dog/dog-0', suffix: '.png'
    // });

    // self.anims.create({ key: 'dog_attack', frames: dogAttack, frameRate: 3, repeat: -1, hideOnComplete: true});
}

// 鍵盤按鍵
function setupKeyboard(self) {

    //  Create a Key object we can poll directly in a tight loop

    WKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    AKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    SKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    DKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    SpaceKey = self.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

    // 使用參考
    // if (SpaceKey.isUp)
    // {
    //     alien1.isJump = false;
    // }

    // if (AKey.isDown) {
    //     alien1.setVelocityX(-250);
    // }else if (DKey.isDown) {
    //     alien1.setVelocityX(250);
    // } else {
    //     alien1.setVelocityX(0);
    // }
}

function getPlayerByName(playerName) {
    var _player = players.getChildren().filter(function(obj){
        return obj.name === playerName;
    });
    return _player[0];
}

function spawnPlayer(self, x, y, playerName, initFrame, idleFrame, isEnemy) {
    var container = self.add.container(x, y);
    var player = new Actor(self, playerName ,0, 0, 'atlas', initFrame, idleFrame);
    player.name = "player";
    container.setSize(player.width, player.height);
    container.add(player);
    var bloodBar = self.add.graphics();
    bloodBar.name = "blood";
    bloodBar.clear();
    bloodBar.fillStyle(0xff0000, 1);
    bloodBar.fillRect(player.width/2*-1, player.height/2*-1, player.width, 10);
    player.bloodBar = bloodBar;
    container.add(bloodBar);
    container.playerBullets = self.physics.add.group({ classType: Bullet, maxSize: 1, runChildUpdate: true });
    container.fireInit = false;
    container.bulletTick = 0;

    container.fire = function(enemy , tick) {
        if (this.bulletTick === 0) {
            this.bulletTick = tick;
        }
        if (Math.abs(this.bulletTick - tick) < 300) {
            console.log(this.bulletTick);

            return;
        }
        var bullet = this.playerBullets.get();
        if (bullet) {
            let playerz = this.getByName('player');
            if (!playerz.alive) {
                console.log(`actor: ${this.name} can't shoot...`);
                return;
            }

            bullet.setActive(true).setVisible(true);
            bullet.fire(this, { x: enemy.x  , y: enemy.y });
            bullet.setScale(0.2, 0.2);
            bullet.setSize(0.2, 0.2);
            bullet.body.setCollideWorldBounds(true);
            bullet.body.onWorldBounds = true;

            // this.tweens.add({
            //     targets: bullet,
            //     rotation: ,
            //     ease: 'Linear',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
            //     duration: 200,
            //     repeat: -1,            // -1: infinity
            // });

            this.bulletTick = 0;
            //
            if (container.fireInit === false) {
                container.fireInit = true;
                self.physics.add.overlap(container.playerBullets, enemies, function (bullet, playerC) {
                    var player = playerC.getByName("player");
                    var ex = self.add.sprite(playerC.x, playerC.y, 'atlas', 'sprites/beer_broken_01.png').play('beer_broken');
                    ex.setScale(0.5, 0.5);
                    MGlassBrk.play();
                    ws.actionHitBose();
                    playerC.scaleTween.restart();
                    player.Injured(2);
                    if (!player.alive) {
                        playerC.setVisible(false);
                        playerC.setActive(false);
                        self.scene.pause();
                        
                        // playerC.MExplosion.play();
                    }
                    // // // console.log(blood);
                    bullet.setPosition(1000,1000);
                    bullet.clear();
                });
            }
        }
    }
    container.name = playerName;
    return container;
}